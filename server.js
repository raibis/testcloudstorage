require('dotenv').config()
const {format} = require('util');
const express = require('express')
const {Storage} = require('@google-cloud/storage');
const Multer = require('multer');

const storage = new Storage({
    projectId: process.env.GCP_PROJECT_ID,
    credentials:{
        type: process.env.GCP_TYPE,
        client_email: process.env.GCP_CLIENT_EMAIL,
        client_id: process.env.GCP_CLIENT_ID,
        private_key:  process.env.GCP_PRIVATE_KEY?.replace(/\\n/g, "\n"),
    }
});

const userId = 'user1234'

const app = express()

app.use(express.json());

const multer = Multer({
  storage: Multer.memoryStorage(),
  limits: {
    fileSize: 5 * 1024 * 1024,
  },
});

const bucket = storage.bucket(process.env.GCP_BUCKET);

app.post('/',  multer.single('file'), (req, res, next) => {
if (!req.file) {
    res.status(400).send('No file uploaded.');
    return;
  }
  const blob = bucket.file(`${userId}/${req.file.originalname}`);
  const blobStream = blob.createWriteStream({
    resumable: false,
  });

  
  blobStream.on('error', err => {
    next(err);
  });

  blobStream.on('finish', () => {
    const publicUrl = format(
      `https://storage.googleapis.com/${bucket.name}/${blob.name}`
    );
    res.status(200).send(publicUrl);
  });

  blobStream.end(req.file.buffer);
})

const port = process.env.PORT
app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})